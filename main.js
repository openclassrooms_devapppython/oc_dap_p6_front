const baseUrl = 'http://127.0.0.1:8000'
const categoriesToFetch = ['Crime', 'Drama', 'Sport']


const elementsToDisplay = () => {
    const screenWidth = window.innerWidth
    if (screenWidth <= 768) {
        return 2
    } else if (screenWidth <= 1200) {
        return 4
    }
    return 6
}

const numberOfMoviesDisplayed = new Array(5).fill(elementsToDisplay())


const fetchDataAndToggleModal = async (e) => {
    let movieName = findNearestElement(e.target, 'h3').innerText
    let fetchUrl = baseUrl + `/api/v1/titles/?title=${movieName}`
    let data = await fetch(fetchUrl)
    data = await data.json()
    fetchUrl = data.results[0].url
    data = await fetch(fetchUrl)
    data = await data.json()
    const dialog = document.querySelector('dialog')
    dialog.innerHTML = `
            <article>
                <header>
                    <div>
                        <h3>${data.title}</h3>
                        <p>${data.year} - ${data.genres}</p>
                        <p>PG-${data.rated} - ${data.duration} minutes (${data.countries})</p>
                        <p>IMDB Score : ${data.imdb_score}</p>
                        <p>Réalisé par : <br> ${data.directors}</p>
                    </div>
                    <div>
                        <img src="${data.image_url}" alt="Movie poster of ${data.title}">
                    </div>
                </header>
                <section>
                    <p>${data.long_description}</p>
                    <p>Avec : <br> ${data.actors}</p>
                </section>
                <footer>
                    <button id="close-modal">Fermer</button>
                </footer>
            </article>
        `
    const closeButton = document.querySelector('#close-modal')
    closeButton.addEventListener('click', () => {
        dialog.close()
    })
    dialog.showModal()
}

const findNearestElement = (element1, element2) => {
    let previousElement = element1.previousElementSibling;
    while (previousElement) {
        if (previousElement.tagName.toLowerCase() === element2) {
            return previousElement;
        }
        previousElement = previousElement.previousElementSibling;
    }
    return null;
}


// Best movie section
const fetchBestMovie = async () => {
    const bestMovie = document.querySelector('#best-movie')
    const response = await fetch(baseUrl + '/api/v1/titles/?sort_by=-imdb_score')
    const data = await response.json()
    const bestMovieUrl = data.results[0].url
    const movieResponse = await fetch(bestMovieUrl)
    const movieData = await movieResponse.json()
    bestMovie.innerHTML = `
    <img src="${movieData.image_url}" alt"L'affiche de ${movieData.title}">
    <div>
        <h3>${movieData.title}</h3>
        <p>${movieData.description}</p>
        <button class="details">Details</button>
    </div>
    `
    const detailsButton = document.querySelector('.details')
    detailsButton.addEventListener('click', (event) => fetchDataAndToggleModal(event))

    const image = document.querySelector('img')
    image.addEventListener('error', (event) => {
        event.target.src = '/logo.png'
        event.target.alt = 'Logo de JustStreamIt en placeholder car l\'image est inaccessible.'
    })
}

const fetchBestMovies = async () => {
    const section = document.querySelector('.category-0')
    let response = await fetch(baseUrl + '/api/v1/titles/?sort_by=-imdb_score')
    let data = await response.json()
    let dataList = [...data.results]
    dataList.shift()
    if (data.next) {
        response = await fetch(data.next)
        data = await response.json()
        dataList = [...dataList, ...data.results]
    }
    if (dataList.length > 6) {
        dataList = dataList.slice(0, 6)
    }
    let innerHTML = ''
    dataList.forEach((data, index) => {
        innerHTML += `
        <article ${index < numberOfMoviesDisplayed[0] ? '' : 'hidden'}>
            <img src="${data.image_url}"
                 alt="Film poster of ${data.title}">
            <div>
                <h3>${data.title}</h3>
                <button class="details">Details</button>
            </div>
        </article>
        `
    })

    if (numberOfMoviesDisplayed[0] < 6) {
        innerHTML += `<button class="see-more" id="see-more${0}">Voir plus</div>`
    }



    const category = document.querySelector(`.category-${0}`)
    category.innerHTML = innerHTML

    const seeMoreButton = document.querySelector(`#see-more${0}`)
    if (seeMoreButton) {
        seeMoreButton.addEventListener('click', () => seeMore(0))
    }

    const detailsButtons = document.querySelectorAll('.details')
    detailsButtons.forEach((button) => {
        button.addEventListener('click', (event) => fetchDataAndToggleModal(event))
    })

    const images = document.querySelectorAll('img')
    images.forEach((image) => {
        image.addEventListener('error', (event) => {
            event.target.src = '/logo.png'
            event.target.alt = 'Logo de JustStreamIt en placeholder car l\'image est inaccessible.'
        })
    })
}

// Categories
const fetchEntireCategory = async (category_number, category_name, modify = true) => {
    const fetchUrl = baseUrl + `/api/v1/titles/?genre=${category_name}&sort_by=-imdb_score`
    let response = await fetch(fetchUrl)
    let data = await response.json()
    let dataList = [...data.results]
    if (data.next) {
        response = await fetch(data.next)
        data = await response.json()
        dataList = [...dataList, ...data.results]
    }
    if (dataList.length > 6) {
        dataList = dataList.slice(0, 6)
    }
    let innerHTML = ''
    dataList.forEach((data, index) => {
        innerHTML += `
        <article ${index < numberOfMoviesDisplayed[category_number] ? '' : 'hidden'}>
            <img src="${data.image_url}"
                 alt="Film poster of ${data.title}">
            <div>
                <h3>${data.title}</h3>
                <button class="details">Details</button>
            </div>
        </article>
        `
    })

    if (numberOfMoviesDisplayed[category_number] < 6) {
        innerHTML += `<button class="see-more" id="see-more${category_number}">Voir plus</div>`
    }



    const category = document.querySelector(`.category-${category_number}`)
    category.innerHTML = innerHTML
    
    const seeMoreButton = document.querySelector(`#see-more${category_number}`)
    if (seeMoreButton) {
        seeMoreButton.addEventListener('click', () => seeMore(category_number))
    }

    const closestH2 = findNearestElement(category, 'h2')
    if (closestH2 && modify) {
        closestH2.innerText = category_name
    }

    const detailsButtons = document.querySelectorAll('.details')
    detailsButtons.forEach((button) => {
        button.addEventListener('click', (event) => fetchDataAndToggleModal(event))
    })

    const images = document.querySelectorAll('img')
    images.forEach((image) => {
        image.addEventListener('error', (event) => {
            event.target.src = '/logo.png'
            event.target.alt = 'Logo de JustStreamIt en placeholder car l\'image est inaccessible.'
        })
    })
}


// Others
const fetchGenres = async () => {
    let fetchUrl = baseUrl + '/api/v1/genres/'
    let genresList = []

    let dataToFetch = true
    while (dataToFetch) {
        let response = await fetch(fetchUrl)
        let data = await response.json()
        genresList = [...genresList, ...data.results]
        if (data.next) {
            fetchUrl = data.next
        } else {
            dataToFetch = false
        }
    }

    genresList = genresList.filter((genre) => !categoriesToFetch.includes(genre.name))
    let options = ""
    const firstGenre = genresList.shift()
    await fetchEntireCategory('4', firstGenre.name, false)
    options += `<option value="${firstGenre.name.toLowerCase()}" >${firstGenre.name}</option>`
    genresList.forEach((genre) => {
        options += `<option value="${genre.name.toLowerCase()}" >${genre.name}</option>`
    })


    const select = document.querySelector('#category')
    select.innerHTML = options
    select.addEventListener('change', () => {
        fetchEntireCategory('4', select.value, false)
        numberOfMoviesDisplayed[3] = elementsToDisplay()
        if (elementsToDisplay() < 6) {
            const seeMoreButton = document.querySelector('#see-more4')
            seeMoreButton.hidden = false
        }
    })
}

const seeMore = (category_number) => {
    const category = document.querySelector(`.category-${category_number}`)
    const articles = category.querySelectorAll('article')
    numberOfMoviesDisplayed[category_number] += 2
    for (let i = 0; i < numberOfMoviesDisplayed[category_number]; i++) {
        articles[i].hidden = false
    }
    if (numberOfMoviesDisplayed[category_number] === 6) {
        const seeMoreButton = document.querySelector(`#see-more${category_number}`)
        seeMoreButton.hidden = true
    }
}

// Main
await fetchBestMovie()
await fetchBestMovies()
await fetchEntireCategory(1, categoriesToFetch[0])
await fetchEntireCategory(2, categoriesToFetch[1])
await fetchEntireCategory(3, categoriesToFetch[2])
await fetchGenres()
